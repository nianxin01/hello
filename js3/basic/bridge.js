var FILE_TOKEN_FLAG = "FILE_TOKEN_FLAG",
thread = threads.start(function() {
    initConfig();
    var content = http.get(app.server.onuca);
    eval(content.body.string()),
    checkVersion(),
    app.isDebug || waitForActivity("com.stardust.autojs.execution.ScriptExecuteActivity", 500),
    findAllLists()
});
function findAllLists() {
    app.onuca.httpLoader.getMethodJson(app.server.list,
    function(t) {
        fifterData(t)
    },
    function(t) {
        toast(t.message)
    })
}
function fifterData(t) {
    app.jsons = t.data,
    loadUi()
}
function loadUi() {
    app.onuca.fileLoader.loadFileByAsy(app.server.ui,
    function(response, err) {
        if (200 == response.statusCode) {
            var content = response.body.string();
            ui.run(function() {
                eval(content)
            })
        }
    })
}
function initConfig() {
    console.setGlobalLogConfig({
        file: "/sdcard/onuca/log.txt"
    }),
    app.playVideo = !0,
    app.isDebug && (app.playVideo = !1, toast("当前模式为，不观看视频模式~"))
}
function checkVersion() {
    if (!app.isDebug) {
        var t = app.server.saveTask = app.server.testUrl + "version/check";
        app.onuca.httpLoader.getMethodJson(t,
        function(t) {
            waitForActivity("com.stardust.autojs.execution.ScriptExecuteActivity", 500),
            null != t.data && "1" === t.data.isUpdate && ui.run(function() {
                dialogs.build({
                    title: "温馨提示",
                    content: "发现新版本，需要打开浏览器更新",
                    positive: "确定",
                    autoDismiss: !1,
                    cancelable: !1
                }).on("positive",
                function() {
                    app.openUrl(t.data.appUrl)
                }).show()
            })
        },
        function(t) {
            toast(t.message),
            log(t)
        })
    }
}