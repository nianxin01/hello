var globalColor = "#CC0000";
ui["statusBarColor"](globalColor);
ui.layout(
    <frame>
        <vertical>
            <appbar>
                <toolbar id="toolbar" bg="#CC0000" title="自动阅读" />
            </appbar>
            <list id="list" marginBottom="10" >
                <card w="*" h="100" margin="10 5" cardCornerRadius="2dp"
                    cardElevation="1dp" foreground="?selectableItemBackground">
                    <horizontal gravity="center_vertical">
                        <View bg="#cc0000" h="*" w="10" />
                        <vertical padding="10 8" h="auto" w="0" layout_weight="1">
                            <text id="title" text="{{this.appName}}" textColor="#222222" textSize="16sp" maxLines="1" />
                            <text text="{{this.appPackageName}}"  margin="0 10"  textColor="#999999" textSize="14sp" maxLines="1" />
                        </vertical>
                        <radiogroup  marginLeft="4" marginRight="15" orientation="horizontal" id="timeGroup"  >
                            <radio text="白天" id="day"  checked="{{this.status==0}}"/>
                            <radio text="晚上" id="night"  checked="{{this.status==1}}"/>
                            <radio text="停止" id="none" checked="{{this.status==2}}"/>
                        </radiogroup>
                    </horizontal>
                </card>
            </list>

        </vertical>
        <fab id="add" w="auto" h="auto" src="http://www.autojs.org/assets/uploads/profile/3-profileavatar.png"
            margin="10 10 15 50" layout_gravity="bottom|right" tint="#ffffff"  />
    </frame>
);

var menuContents = ["一键安装", "运行时间配置", "运行环境校验", "关于"];

if(app['isDebug']){
    menuContents.push("运行环境设置")
}

ui.emitter.on("create_options_menu", menu => {
    for (var i = 0; i < menuContents.length; i++) {
        menu.add(menuContents[i]);
    }
});

ui.emitter.on("options_item_selected", (e, item) => {
    if (app["isRuning"] != null && app["isRuning"]) {
        dialogs.build({
            title: "温馨提示",
            content: "当前脚本正在运行，无法修改配置",
            positive: "知道了"
        }).show();
    } else {
        switch (item.getTitle()) {
            case menuContents[0]:
                confirm("接下来会自动安装SDCard/cc文件夹中的APK，安装完成后，需要重新启动APP。确定？").then(function (values) {
                    if (values) {
                        app['onuca']['autoInstall'].start();
                    }
                });
                break;
            case menuContents[1]:
                var checkedIndex = app['storage'].get("runMode", "2");
                dialogs.singleChoice("请选择程序运行周期", ["09点-23点", "10点-22点", "全天运行"], checkedIndex, function (i) {
                    if (i < 0) return;
                    app['storage'].put("runMode", i + "");
                    toast("修改成功");
                })
                break;
            case "运行环境设置":
                var checkedIndex = app['storage'].get("struck", "0") === "0" ? 1000 : 0;
                dialogs.multiChoice("设置程序运行环境", ["当出现异常时,是否阻塞切换程序"], [checkedIndex], function (indexs) {
                    if (indexs.length === 0) {
                        app['storage'].put("struck", "0");
                    } else {
                        app['storage'].put("struck", "1");
                    }
                    toast("修改成功");
                })

                break;
            case menuContents[2]:
                confirm("接下来需要对平台进行校验，确定？").then(function (values) {
                    if (values) {
                        app['onuca']['checkTools'].start(models);
                    }
                });
                break;
            case menuContents[3]:
                alert("关于", "自动阅读1.0版本");
                break;
        }
    }
    e.consumed = true;
});
activity.setSupportActionBar(ui.toolbar);



var models = getListDatas();
ui.list.setDataSource(models);



ui.list.on("item_bind", function (itemView, itemHolder) {
    itemView.day.on("check", function (check) {
        if(check){
            var item = itemHolder.item;
            item.status ="0";
            app['onuca']['taskStorage'].saveTaskStatus(models);
        }
    });
    itemView.night.on("check", function (check) {
        if(check){
            var item = itemHolder.item;
            item.status ="1";
            app['onuca']['taskStorage'].saveTaskStatus(models);
        }
    });
    itemView.none.on("check", function (check) {
        if(check){
            var item = itemHolder.item;
            item.status ="2";
            app['onuca']['taskStorage'].saveTaskStatus(models);
        }
    });
   
});


ui.list.on("item_click", function (item, i, itemView, listView) {
    var appName=item.appName;
    var target=app["onuca"]["taskDetailStorage"].fetchTaskDetailInfo(appName);
    var duration = Math.floor((target.finalDuration / 60) * 100) / 100;
    var i = dialogs.select(
        appName + "运行详情", 
        "总共运行次数："+ target.runCount + " 次", 
        "错误运行次数："+ target.errorFinishCount + " 次", 
        "阅读视频数量: "+ target.videoCount  + " 次", 
        "阅读文章数量: "+ target.articleCount + " 次",
        "今日总共运行时间: "+ duration + " 分", 
        "今日运行获得金币: "+ target.finalCoin + " 个" 
        
        );

});

ui.add.on("click", () => {
    app['onuca']['onuca'].doExecute2UI();
})



// threads.start(function(){
//     events.observeNotification();
//     events.onNotification(function(notification){
//     wechat.filter(notification)
//     });
// });



function getListDatas() {
    var models = app['onuca']['taskStorage'].getFinalAllTask(app['jsons']);
    app["onuca"]["taskDetailStorage"].init(app['jsons']);
    for (var i = 0; i < models.length; i++) {
        models[i]["type"] = "news"
        models[i]["runCount"] = 0;
    }
    return models;
}

